using System;
using System.Threading.Tasks;
using Actio.Common.Commands;
using Actio.Common.Events;
using Actio.Common.Exceptions;
using Actio.Services.Identity.Services;
using Microsoft.Extensions.Logging;
using RawRabbit;

namespace Actio.Services.Identity.Handlers
{
    public class CreateUserHandler : ICommandHandler<CreateUser>
    {

        private readonly IBusClient _client;
        private readonly IUserService _userService;
        private readonly ILogger _logger;

        public CreateUserHandler(IBusClient client, IUserService userService, ILogger<CreateUserHandler> logger)
        {
            this._client = client;
            this._userService = userService;
            this._logger = logger;
        }

        public async Task HandleAsync(CreateUser command)
        {
            _logger.LogInformation($"Creating user: {command.Email}");
            try
            {
                await _userService.RegisterAsync(command.Email, command.Password, command.Name);
                await _client.PublishAsync(
                    new UserCreated(command.Email, command.Name));
                return;
            }
            catch (ActioException ex)
            {
                await _client.PublishAsync(new CreateUserRejected(command.Email, ex.Code, ex.Message));
                _logger.LogError(ex.Message);
            }
            catch (Exception ex)
            {
                await _client.PublishAsync(new CreateUserRejected(command.Email, "error", ex.Message));
                _logger.LogError(ex.Message);
            }
        }
    }
}
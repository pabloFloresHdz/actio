using System;
using System.Threading.Tasks;
using Actio.Common.Commands;
using Actio.Common.Events;
using Actio.Common.Exceptions;
using Actio.Services.Activities.Services;
using Microsoft.Extensions.Logging;
using RawRabbit;

namespace Actio.Services.Activities.Handlers
{
    public class CreateActivityHandler : ICommandHandler<CreateActivity>
    {
        private readonly IBusClient _client;
        private readonly IActivityService _activityService;
        private readonly ILogger _logger;

        public async Task HandleAsync(CreateActivity command)
        {
            _logger.LogInformation($"Creating activity: {command.Category} {command.Name}");
            try
            {
                await _activityService.AddAsync(command.Id, command.UserId, command.Name,
                    command.Category, command.Description, command.Date);
                await _client.PublishAsync(
                    new ActivityCreated(command.Id, command.UserId, command.Name,
                        command.Category, command.Description, command.Date));
                return;
            }
            catch (ActioException ex)
            {
                await _client.PublishAsync(new ActivityCreatedRejected(command.Id, ex.Code, ex.Message));
                _logger.LogError(ex.Message);
            }
            catch (Exception ex)
            {
                await _client.PublishAsync(new ActivityCreatedRejected(command.Id, "error", ex.Message));
                _logger.LogError(ex.Message);
            }

        }

        public CreateActivityHandler(IBusClient client, IActivityService activityService, ILogger logger)
        {
            _client = client;
            _activityService = activityService;
            _logger = logger;
        }
    }
}
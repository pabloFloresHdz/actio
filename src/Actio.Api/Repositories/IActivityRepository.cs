using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Actio.Api.Domain.Model;

namespace Actio.Api.Repositories
{
    public interface IActivityRepository
    {
        Task<Activity> GetAsync(Guid id);
        Task AddAsync(Activity model);
        Task<IEnumerable<Activity>> BrowseAsync();
        Task<IEnumerable<Activity>> BrowseAsync(Guid userId);
    }
}
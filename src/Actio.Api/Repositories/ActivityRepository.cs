using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Actio.Api.Domain.Model;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace Actio.Api.Repositories
{
    public class ActivityRepository : IActivityRepository
    {
        private IMongoDatabase _database;
        
        public ActivityRepository(IMongoDatabase database)
        {
            _database = database;
        }

        public async Task AddAsync(Activity model)
        {
            await this.Collection.InsertOneAsync(model);
        }

        public async Task<IEnumerable<Activity>> BrowseAsync()
        {
            //return await this.Collection.AsQueryable().Where(x=>x.userId == id).ToListAsync();
            return await this.Collection.AsQueryable().ToListAsync();
        }

        public async Task<IEnumerable<Activity>> BrowseAsync(Guid userId)
        {
            //return await this.Collection.AsQueryable().Where(x=>x.userId == id).ToListAsync();
            return await this.Collection.AsQueryable().Where(x => x.UserId == userId).ToListAsync();
        }

        public async Task<Activity> GetAsync(Guid id)
        {
            return await this.Collection.AsQueryable().FirstOrDefaultAsync(activity => activity.Id == id);
        }

        private IMongoCollection<Activity> Collection => _database.GetCollection<Activity>("Activities");
    }
}
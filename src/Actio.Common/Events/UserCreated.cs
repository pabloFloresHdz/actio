namespace Actio.Common.Events
{
    public class UserCreated : IEvent
    {
        public string Email { get; }
        public string Name { get; }

        protected UserCreated(object id)
        {

        }
        public UserCreated(string email, string name)
        {
            Email = email;
            Name = name;
        }
    }
}
using System;

namespace Actio.Common.Events
{
    public class ActivityCreatedRejected : IRejectedEvent
    {
        public Guid UserId { get; }
        public string Reason { get; }
        public string Code { get; }

        protected ActivityCreatedRejected()
        {

        }

        public ActivityCreatedRejected(Guid userId, string code, string reason)
        {
            UserId = userId;
            Reason = reason;
            Code = code;
        }
    }
}
using System;
using System.Security.Claims;
using Actio.Api.Controllers;
using Actio.Api.Repositories;
using Actio.Common.Commands;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using RawRabbit;
using Xunit;

namespace Actio.Api.Tests.Unit.Controllers {
    public class ActivitiesControllerTests {
        [Fact]
        public async void activities_controller_post_should_return_accepted () {
            var busClient = new Mock<IBusClient> ();
            var activitiesRepository = new Mock<IActivityRepository> ();
            var controller = new ActivitiesController (busClient.Object, activitiesRepository.Object);
            var userId = Guid.NewGuid();
            controller.ControllerContext = new ControllerContext{
                HttpContext = new DefaultHttpContext{
                    User = new ClaimsPrincipal(new ClaimsIdentity(
                        new Claim[]{new Claim(ClaimTypes.Name, userId.ToString())}
                    ,"test"))
                }
            };
            var command = new CreateActivity{
                Id = Guid.NewGuid(),
                UserId = userId
            };

            var result = await controller.Post(command);

            var contentResult = result as AcceptedResult;
            contentResult.Should().NotBeNull();
            contentResult.Location.Should().BeEquivalentTo($"activities/{command.Id}");
        }
    }
}